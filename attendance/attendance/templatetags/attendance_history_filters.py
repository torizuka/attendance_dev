from django import template

register = template.Library()

@register.filter
def date(querydict):
    history = querydict.get('date')
    return "" if history is None else history
