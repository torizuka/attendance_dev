from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from django.views.generic import TemplateView
from django.contrib.auth import get_user_model
from .models import Holiday
from .models import History
from .models import User
from django.core.cache import cache
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.shortcuts import redirect

import calendar
import datetime
import datetime as dt

# ログアウト
class LogoutView(TemplateView):
    def get(self, *args, **kwargs):
        logout(self.request)
        url = reverse('attendance:login')
        # return redirect('attendance:login')
        return HttpResponseRedirect(url)

# 時間入力画面
class EnterTimeView(TemplateView):
    template_name = "attendance/entertime.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs) # はじめに継承元のメソッドを呼び出す

        # 現在日時取得
        strdate = '{0:%Y-%m-%d}'.format(datetime.datetime.now()) #test_20170910
        day = self.kwargs.get('day')
        if day is not None:
            strdate = day

        try:
            obj = History.objects.get(user_id=self.request.user.id, date=strdate)
            params = { # <- 渡したい変数を辞書型オブジェクトに格納
                # 'start': obj.start_time,
                'start': obj.start_time.strftime('%H:%M'),
                # 'end': obj.end_time,
                'end': obj.end_time.strftime('%H:%M'),
                'paid': obj.paid,
                'day': strdate
            }
            context=params
            return context

        except History.DoesNotExist:
            params = {
                'day': strdate
            }
            context=params
            return context

class DetailView(generic.DetailView):
    model = Holiday
    template_name = 'attendance/detail.html'


class ResultsView(generic.DetailView):
    model = Holiday
    template_name = 'attendance/results.html'


def daysearch(request, day):
    url = reverse('attendance:day', kwargs={'day': day})
    print(url)
    return HttpResponseRedirect(url)

def regist(request):
    History.regist_history(request.user.id, request.POST['day'], request.POST['start'], request.POST['end'], request.POST['paid'])
    url = reverse('attendance:day', kwargs={'day': request.POST['day']})
    print(url)
    return HttpResponseRedirect(url)

def historyremove(request, id):
    # cache.set('day', day, 60 * 15)  # cache in 15 minutes
    History.remove_history(id)
    return HttpResponseRedirect(reverse('attendance:historylist'))

def get_h_m_s(td):
    m, s = divmod(td.seconds, 60)
    h, m = divmod(m, 60)
    return h, m, s


# 勤務時間確認画面
class HistoryListView(TemplateView):
    model = History
    template_name = "attendance/historylist.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs) # はじめに継承元のメソッドを呼び出す

        #有給日数
        context['paid'] = self.request.user.paid
        history_list = self.model.objects.all().order_by('date')

        #年月指定があった場合は現在日時を使用
        strdate = '{0:%Y-%m-%d}'.format(datetime.datetime.now())
        context['yearmonth'] = strdate.split('-')[0] + '-' + strdate.split('-')[1]
        #年月指定があった場合はフィルタ指定
        if self.request.GET.get('date') is not None:
            strdate = self.request.GET.get('date')
            context['yearmonth'] = strdate

        hislist = []
        object_list =history_list.filter(date__year=strdate.split('-')[0], date__month=strdate.split('-')[1])
        # context['history_list'] = object_list

        #合計時間
        totalworktime = 0
        requiredworkday = 0

        for history in object_list:
            day = history.date.strftime('%Y-%m-%d') + " "

            #業務開始時間
            strstart = day + history.start_time.strftime('%H:%M:%S.000') # 日付を文字列
            start_time = dt.datetime.strptime(strstart, "%Y-%m-%d %H:%M:%S.000") # 文字列を日付
            #業務終了時間
            strend = day + history.end_time.strftime('%H:%M:%S.000')
            end_time = dt.datetime.strptime(strend, "%Y-%m-%d %H:%M:%S.000")

            #昼休憩開始時間
            strlbstart = day + "12:30:00"
            lbstart_time = dt.datetime.strptime(strlbstart, "%Y-%m-%d %H:%M:%S")
            #昼休憩終了時間
            strlbend = day + "13:30:00"
            lbend_time = dt.datetime.strptime(strlbend, "%Y-%m-%d %H:%M:%S")

            #労働時間
            worktime = end_time - start_time

            #通常
            if lbstart_time >= start_time and end_time >= lbend_time:
                worktime = worktime - datetime.timedelta(hours=1) #1時間減算
            #午前半休パターン
            elif lbstart_time >= start_time and end_time <= lbend_time:
                worktime = worktime - (end_time - lbstart_time) #12:30以降の時間減算
            #午後半休パターン
            elif lbstart_time <= start_time and end_time >= lbend_time:
                worktime = worktime - (lbend_time - start_time) #12:30以前の時間減算

            #夕憩開始時間
            strebstart = day + "18:00:00"
            ebstart_time = dt.datetime.strptime(strebstart, "%Y-%m-%d %H:%M:%S")
            #夕憩終了時間
            strebend = day + "18:15:00"
            ebend_time = dt.datetime.strptime(strebend, "%Y-%m-%d %H:%M:%S")

            #通常
            if ebstart_time >= start_time and end_time >= ebend_time:
                worktime = worktime - datetime.timedelta(minutes=15) #15分減算
            #18:00から18:15の間に退勤した場合
            elif end_time >= ebstart_time and end_time <= ebend_time:
                worktime = worktime - (end_time - ebstart_time) #18:15までの時間を減算

            #半有給の場合は実働に3:45を加算
            if history.paid == 0.5:
                worktime = worktime + datetime.timedelta(hours=3)
                worktime = worktime + datetime.timedelta(minutes=45)

            totalworktime = totalworktime + worktime.total_seconds()

            #稼働時間
            h, m, s = get_h_m_s(worktime)
            if m < 10:
                strWork = str(h) + ":0" + str(m)
            else:
                strWork = str(h) + ":" + str(m)
            history.daytime = strWork

            hislist.append(history)

            #コアタイム開始時間
            strcorestart = day + "10:30:00"
            corestart_time = dt.datetime.strptime(strcorestart, "%Y-%m-%d %H:%M:%S")
            #コアタイム終了時間
            # strcoreend = day + "15:30:00"
            strcoreend = day + "14:30:00" #育児のため時種変、コアタイム15:30 -> 14:30
            coreend_time = dt.datetime.strptime(strcoreend, "%Y-%m-%d %H:%M:%S")
            #出勤日数加算
            if history.paid == 0.0 and corestart_time < start_time and coreend_time < end_time:
                requiredworkday = requiredworkday + 0.5
            elif history.paid == 0.0 and corestart_time > start_time and coreend_time > end_time:
                requiredworkday = requiredworkday + 0.5
            else:
                requiredworkday = requiredworkday + 1

        context['history_list'] = hislist

        #選択月のに数を取得
        _, lastday = calendar.monthrange(int(strdate.split('-')[0]),int(strdate.split('-')[1]))

        #休日数取得
        holiday = Holiday.objects.get(month=int(strdate.split('-')[1]))

        #必要出勤日数算出
        context['workday'] = lastday - holiday.holiday_count

        #選択月休日数
        context['holidaycount'] = holiday.holiday_count

        #出勤日数算出
        # context['requiredworkday'] = len(object_list)
        context['requiredworkday'] = requiredworkday

        #出勤日数差分算出
        context['workdaydiff'] = lastday - holiday.holiday_count - requiredworkday

        #実労働時間算出
        td = datetime.timedelta(seconds=totalworktime)
        h, m, s = get_h_m_s(td)
        strColon = ":"
        if m < 10:
            strColon = ":0"

        context['requiredtime'] = str(h + 24 * td.days) + strColon + str(m)


        #必要労働時間算出
        worktime = datetime.timedelta(hours=7)
        worktime = worktime + datetime.timedelta(minutes=30)
        worktime = worktime * (lastday - holiday.holiday_count)
        h, m, s = get_h_m_s(worktime)
        strColon = ":"
        if m < 10:
            strColon = ":0"
        context['worktime'] = str(h + 24 * worktime.days) + strColon + str(m)

        #労働時間過不足算出
        worktimediff = worktime - td
        h, m, s = get_h_m_s(worktimediff)

        if h + 24 * worktimediff.days < 0:
            worktimediff = td - worktime
            h, m, s = get_h_m_s(worktimediff)
            strColon = ":"
            if m < 10:
                strColon = ":0"
            context['worktimediff'] = "超過" + str(h + 24 * worktimediff.days) + strColon + str(m)

        else:
            strColon = ":"
            if m < 10:
                strColon = ":0"
            context['worktimediff'] = str(h + 24 * worktimediff.days) + strColon + str(m)

        return context

# 勤怠一覧取得
def search(search):

    return HttpResponseRedirect(reverse('attendance:historylist'))
