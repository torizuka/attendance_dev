from django.urls import path, include # 追加
from . import views

app_name = 'attendance'
urlpatterns = [
    # path('', views.IndexView.as_view(), name='index'),
    path('', views.EnterTimeView.as_view(), name='index'), # 時間入力画面
    path('<str:day>', views.EnterTimeView.as_view(), name='day'),
    path('accounts/', include('django.contrib.auth.urls')), #
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    path('regist/', views.regist, name='regist'),
    path('daysearch/<str:day>', views.daysearch, name='daysearch'),
    path('historylist/', views.HistoryListView.as_view(), name='historylist'), # 勤務時間確認画面
    path('historysearch/', views.search, name='search'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('historyremove/<str:id>', views.historyremove, name='historyremove'),
]
