from django.db import models
from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.contrib.auth.base_user import BaseUserManager
# Create your models here.

class Holiday(models.Model):
    month = models.IntegerField(verbose_name='月')
    holiday_count = models.IntegerField(verbose_name='休日数', default=0)

class History(models.Model):
    user_id = models.IntegerField(verbose_name='ユーザID')
    date = models.DateField(verbose_name='日時')
    start_time = models.TimeField(verbose_name='開始時間')
    end_time = models.TimeField(verbose_name=' 終了時間')
    paid = models.DecimalField(verbose_name='有休数', max_digits=3, decimal_places=1, default=0.0)

    def regist_history(add_user_id, add_date, add_start, add_end, add_paid):
        strdate = add_date.replace('/', '-')
        # h = History.objects.get(user_id=add_user_id, date=strdate)
        # print(h.end_time)
        # return History.objects.create(user_id=add_user_id, date=strdate, start_time=add_start, end_time=add_end, paid=add_paid)

        return History.objects.update_or_create(
            # ユニークな値
            user_id=add_user_id, date=strdate,
            # 更新もしくは新規で追加したい値
            defaults={
                "user_id": add_user_id,
                "date": strdate,
                "start_time": add_start,
                "end_time": add_end,
                "paid": add_paid
            },
        )

    def remove_history(id):
        return History.objects.filter(id=id).delete()


class UserManager(BaseUserManager):
    """ユーザーマネージャー."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a user with the given username, email, and
        password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)

        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """カスタムユーザーモデル."""

    email = models.EmailField(_('email address'), unique=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=150, blank=True)

    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_(
            'Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )

    #有休数
    paid = models.DecimalField(verbose_name='有休数', max_digits=3, decimal_places=1, default=0.0)

    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        """Return the first_name plus the last_name, with a space in
        between."""
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)
